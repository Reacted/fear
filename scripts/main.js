const fearDatabase = [
  {
    title: "sleep",
    phobia: "Somniphobia",
    desc:
      "The fear of sleep is related to fear of the unknown. Often the sufferer is terrified of what might happen if he/she falls asleep"
  },
  {
    title: "light",
    phobia: "Photophobia",
    desc:
      "As a medical symptom, photophobia is not a morbid fear or phobia, but an experience of discomfort or pain to the eyes due to light exposure or by presence of actual physical sensitivity of the eyes, though the term is sometimes additionally applied to abnormal or irrational fear of light such as heliophobia."
  },
  {
    title: "meat",
    phobia: "Carnophobia",
    desc:
      "Carnophobia is the extreme and irrational fear of meat. Individuals suffering from carnophobia have difficulty dealing with meat in any way shape or form. Obviously, this can make even small things like going to the supermarket, watching TV, or even driving through the countryside very traumatic."
  },
  {
    title: "time",
    phobia: "Chronophobia",
    desc:
      "Chronophobia is a specific psychological phobia which manifests itself as a persistent, abnormal and unwarranted fear of time or of the passing of time. A related but much rarer phobia is chronomentrophobia, the irrational fear of clocks and watches."
  },
  {
    title: "stealing",
    phobia: "Cleptophobia",
    desc:
      "Cleptophobia is known as the fear of stealing which has two forms; one is the fear of being stolen from or robbed and the other is the fear of stealing from."
  },
  {
    title: "computer",
    phobia: "Cyberphobia",
    desc:
      'Cyberphobia is a concept introduced in 1980, described as a specific phobia expressed as "an irrational fear of or aversion to computers" or, more generally, a fear and/or inability to learn about new technologies.'
  }
];

const searchQuery = {
  text: ""
};

// generate queried result
document.querySelector("#fearQuery").addEventListener("submit", (e) => {
  e.preventDefault();
  searchQuery.text = e.target.elements.fear.value;
  e.target.elements.fear.value = "";
  searchResult(fearDatabase, searchQuery);
});