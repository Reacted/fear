const searchResult = (dbase, query) => {
  const fear = dbase.filter(fear => {
    return fear.title.toLowerCase().includes(query.text.toLocaleLowerCase());
  });

  // prevent showing the database if nothing was entered
  if (query.text === "") {
    return (document.querySelector("#result").innerHTML = `
      <p class="no-text">
        Are you afraid of entering a real text?
      </p>`);
  } else if (fear.length === 0) {
    // when fear is an empty array [] meaning that filter did not find that query!
    notFound(query);
  } else {
    result(fear);
  }
};

// generate the result
const result = fear => {
  fear.forEach(keys => {
    const renderText = `<div class="found">
      <h5>Fear of <em>${keys.title}</em> is known as <strong>${
      keys.phobia
    }</strong></h5>
      <p lass="desc">${keys.desc}</p>
      </div>`;

    // clear the display after each submit event
    clearDisplay();

    generateDOM(renderText);
  });
};

// handle user search words that aren't found
const notFound = querys => {
  const message = `<div class="not_found">
  <h5>Oppss!! your search for '${querys.text}' isn't currently in our database.
  </h5>
  <p>You can get more info about '${querys.text}' from
  <a href="google.com/${querys.text}">here</a>.
  </p>
  <p>Thank you for using our app</p>
  </div>`;

  // clear the display after each submit event
  clearDisplay();

  generateDOM(message);
};

// generate the DOM structure
const generateDOM = msg => {
  const newEl = document.createElement("div");
  const learnMore = document.createElement("button");
  newEl.innerHTML = msg;
  learnMore.textContent = "Learn More";
  // set class attr for button
  learnMore.setAttribute("class", "btn btn-result");
  document.querySelector("#result").appendChild(newEl);
  document.querySelector("#result").appendChild(learnMore);
};

// Clear the display after each rendering
const clearDisplay = () => {
  // clear the display after each submit event
  document.querySelector("#result").innerHTML = "";
};
